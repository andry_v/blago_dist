(function(){
    'use strict';

    angular
        .module('app')
        .controller('MainCtrl', MainCtrl);

    function MainCtrl($scope){

        var vm = this;

        vm.activeTab   = 1;

        vm.spinnerLoading = false;

        vm.stateModel = false;

        vm.closeMenu = () => {
            vm.stateModel = false;
            vm.menuDropDown.isFirstOpen = false;
            vm.menuDropDown.isSecondOpen = false;
        }
        
        vm.activateTab = $event => {
            let el = $event.target || $event.srcElement;
            removeActiveTab();
            $(el).addClass('active-tab');
        };
        
        function removeActiveTab(){
            $('.nav-tabs a').removeClass('active-tab');
        }

        vm.sliderImg = [
            'img/banners/slide_5.jpg',
            'img/banners/slide_4.jpg',
            'img/banners/slide_3.jpg',
            'img/banners/slide_2.jpg',
            'img/banners/slide_1.jpg'
        ];

        vm.sliderActiveIdx = 0;

        vm.flickityOptions = {
            initialIndex: 3
        };

        vm.onSlickLoad = () => true;

        vm.menuDropDown = {
            isFirstOpen: false,
            isSecondOpen: false,
            aboutPage: {
                first: false,
                second: false,
                third: false,
                fourth: false
            },
            payCreditsPage: {
                first: false,
                second: false
            },
            techZalogPage: {
                first: false,
                second: false,
                third: false
            },
            nearestOffice: {
                first: false
            },
            officeAddress: {
                first: false,
                search: {
                    first: false,
                    second: false
                }
            }
        }
    }

})();
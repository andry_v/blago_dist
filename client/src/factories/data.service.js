(() => {

	angular
		.module('app')
		.factory('client', clientData);

	function clientData() {

		var vm = this;

		var service = {
			gGold: getGoldTypes,
			gPers: getPercentage,
			gSilver: getSilver,
			gTechTypes: typesStateToArr,
			gDayNum: dayNumbers,
			gYearNum: yearNumbers,
			exStIco: externalStateIcons
		};

		return service;

		vm.data = {
			goldType: {
				375: 378,
				500: 504,
				583: 588,
				585: 590,
				750: 756,
				850: 857,
				900: 907
			},
			prcForUsg: {
				0: 0.66,
				5000: 0.63,
				10000: 0.61
			},
			silver: {

			},
			tech: {
				types: typesStateToArr()
			}
		};

		function typesStateToArr(){
			var arr = [];
			angular.forEach(typesState, (val, key) => {
				arr.push({
					name: key,
					state: val
				});
			});
			return arr;
		};

		function getGoldTypes() {
			return vm.data.goldType;
		}

		function getPercentage() {
			return data.prcForUsg;
		}

		function getSilver(){

		}

		function dayNumbers(days){
			return Array.from({length: days}, (v, k) => k+1);
		}

		function monthNumbers() {
			let tmpDate = new Date();
			return Array.from({length: 12}, (v, k) => {
							  tmpDate.setMonth(k);
							  return tmpDate.toDateString().slice(4, 7);
						  });
		}

		function yearNumbers(){
			return Array.from({length: 100}, (v, k) => k + 1929);
		}

		function externalStateIcons(){ 
			return Array.from({length: 5}, (v, k) => { 
				return {stateOn: 'fa fa-star icon-bolder', stateOff: 'fa fa-star'} 
			});
		}

		var typesState = {
			"Телефоны": [
				{
					min: 63,
					max: 73
				},
				{
					min: 63,
					max: 73
				},
				{
					min: 67,
					max: 77
				},
				{
					min: 71,
					max: 61
				},
				{
					min: 75,
					max: 85
				}
			],
			"Планшеты": [
				{
					min: 63,
					max: 73
				},
				{
					min: 63,
					max: 73
				},
				{
					min: 67,
					max: 77
				},
				{
					min: 71,
					max: 61
				},
				{
					min: 75,
					max: 85
				}
			],
			"Фотаппараты": [
				{
					min: 63,
					max: 73
				},
				{
					min: 63,
					max: 73
				},
				{
					min: 67,
					max: 77
				},
				{
					min: 71,
					max: 61
				},
				{
					min: 75,
					max: 85
				}
			],
			"Видеокамеры": [
				{
					min: 63,
					max: 73
				},
				{
					min: 63,
					max: 73
				},
				{
					min: 67,
					max: 77
				},
				{
					min: 71,
					max: 61
				},
				{
					min: 75,
					max: 85
				}
			],
			"Телевизоры": [
				{
					min: 66,
					max: 76
				},
				{
					min: 66,
					max: 76
				},
				{
					min: 70,
					max: 80
				},
				{
					min: 74,
					max: 84
				},
				{
					min: 78,
					max: 85
				}
			],
			"GPS-навигатор": [
				{
					min: 43,
					max: 53
				},
				{
					min: 43,
					max: 53
				},
				{
					min: 47,
					max: 57
				},
				{
					min: 51,
					max: 61
				},
				{
					min: 55,
					max: 65
				}
			],
			"MP-3 плеер Apple Ipod": [
				{
					min: 43,
					max: 53
				},
				{
					min: 43,
					max: 53
				},
				{
					min: 47,
					max: 57
				},
				{
					min: 51,
					max: 61
				},
				{
					min: 55,
					max: 65
				}
			],
			"Видеорегистраторы": [
				{
					min: 43,
					max: 53
				},
				{
					min: 43,
					max: 53
				},
				{
					min: 47,
					max: 57
				},
				{
					min: 51,
					max: 61
				},
				{
					min: 55,
					max: 65
				}
			],
			"Игоровая приставка": [
				{
					min: 43,
					max: 53
				},
				{
					min: 43,
					max: 53
				},
				{
					min: 47,
					max: 57
				},
				{
					min: 51,
					max: 61
				},
				{
					min: 55,
					max: 65
				}
			],
			"Мониторы" : [
				{
					min: 43,
					max: 53
				},
				{
					min: 43,
					max: 53
				},
				{
					min: 47,
					max: 57
				},
				{
					min: 51,
					max: 61
				},
				{
					min: 55,
					max: 65
				}
			],
			"Электронная книга": [
				{
					min: 43,
					max: 53
				},
				{
					min: 43,
					max: 53
				},
				{
					min: 47,
					max: 57
				},
				{
					min: 51,
					max: 61
				},
				{
					min: 55,
					max: 65
				}
			],
			"Мелкобытовая техника (Блендеры)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Мелкобытовая техника (Кухонные комбайны)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Мелкобытовая техника (Мультиварки)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Мелкобытовая техника (Мясорубки)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Мелкобытовая техника (Пароварки)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Мелкобытовая техника (Пылесосы)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Мелкобытовая техника (Соковыжималки)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Мелкобытовая техника (Утюги)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Мелкобытовая техника (Хлебопечки)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Мелкобытовая техника (Чайники)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Электроинструмент (Дрели и шуруповерты)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Электроинструмент (Дрели ударные)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Электроинструмент (Лазерный  инструмент)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Электроинструмент (Лобзики электрические)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Электроинструмент (Перфораторы)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Электроинструмент (Пилы электрические, бензиновые)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Электроинструмент (Рубанки электрические)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Электроинструмент (Угловая шлифмашина (Болгарка))": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Электроинструмент (Фен строительный)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Электроинструмент (Фрезеры)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Электроинструмент (Шлифовальные машины)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			],
			"Электроинструмент (Шуруповерты аккумуляторные)": [
				{
					min: 39,
					max: 49
				},
				{
					min: 39,
					max: 49
				},
				{
					min: 41,
					max: 51
				},
				{
					min: 45,
					max: 55
				},
				{
					min: 49,
					max: 59
				}
			]
		};
	}

})();
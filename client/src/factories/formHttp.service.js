(() => {

	angular
		.module('app')
		.factory('formHttp', formHttpProvider)

	formHttpProvider.$inject = ['$http', 'spinnerService', '$log', '$timeout'];

	function formHttpProvider($http, spinnerService, $log, $timeout) {

		var server  	= 'http://localhost:7000';
		//var server  	= 'http://192.168.1.119:7000';

		var service = {
			checkEmail:    	 checkEmail,
			getMarkers:    	 getMarkers,
			sendQuestForm: 	 sendQuestForm,
			sendQuizForm:  	 sendQuizForm,
			getTechTypes: 	 getTechTypes,
			getModelsByBrand: getModelsByBrand,
			getBrandsByType: getBrandsByType
		};

		return service;

		function checkEmail(email) {
			return true;
		}

		function sendQuestForm(user){
			return new Promise((resolve, reject) => {
				try {
					spinnerService.show('questSpinner');
					$http.post('/quest', user).then(resp => {
						resolve();
					}, err => {
						resolve();
					}).finally(() => {
						spinnerService.hide('questSpinner');
					});
				} catch (ex) {
					reject(ex);
				}
			});
		}
		
		function sendQuizForm(data){
			spinnerService.show('quizSpinner');
			return $http.post('/quiz').then(resp => {
				console.log(resp);
			}, err => {
				console.log(err);
			}).finally(() => {
				spinnerService.hide('quizSpinner');
			});
		}

		function getMarkers(dataForMarkers = ''){
			return $http.get('/markers', {
				params: dataForMarkers
			});
		}

		function getTechTypes() {
			return $http.get('/techTypes');
		}

		function getBrandsByType(type) {
			return $http.get('/techTypes/' + type)
						.then(resp => {
							console.log(resp)
							return resp.data;
					}).catch(err => {
						console.log(err);
					});
		}

		function getModelsByBrand(brand) {
			return $http.get('/brands/'+brand)
						.then(resp => {
							console.log(resp);
							return resp.data
						})
						.catch(err => {
							console.error(err);
						})
		}

		function serialize( obj ) {
			return '?'+Object.keys(obj).reduce((a,k) => {
				a.push(k+'='+encodeURIComponent(obj[k]));
				return a;
			},[]).join('&');
		}

	}

})();
$(document).ready(() => {

	// Android 4.2 fix

	// Prestigio PAP5451 Duo navigator.userAgent 
	// Mozilla/5.0 (Linux; U; Android 4.2.1; en-us; 2013022 Build/HM2013022) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30 XiaoMi/MiuiBrowser/1.0
	let isAndroid42 = navigator.userAgent.indexOf('Android 4.2') != -1 || navigator.userAgent.indexOf('Android/4.2') != -1;

	if( isAndroid42 ) {
		
		let styles = document.createElement('link');
		styles.setAttribute('type', 'text/css');
		styles.setAttribute('rel', 'stylesheet');
		styles.setAttribute('href', 'android.css');

		document.head.appendChild(styles);
	}

	if( detectSafari() ){
		/*$('select[name="techType"]').on('touchend', e => {
			var el = e.target || e.currentTarget;
			el.blur();
		});
		$('select[name="techBrand"]').on('blur', e => {
			var el = e.target || e.currentTarget;
			el.blur();
		});
		$('select[name="techModel"]').on('focus', e => {
			var el = e.target || e.currentTarget;
			el.blur();
		});*/
		/*$('html, body').on('click', () => {
			$('select:focus').focus();
		});*/
	}

	let icons   = $('i'),
		selects = $('select'),
		tabs	= $('.nav.nav-tabs'),
		slider	= $('#slider'),
		isIOS	= /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream,
		redB	= $('.red__button'),
		radio	= $('input[name="needCard"]'),
		radioP	= $('input[name="post"]');
	if( icons.length ) {
		icons.removeClass('glyphicon');
	}
	/*if( selects.length ) {
		selects.each(i => {
			let curSel = selects.eq(i),
				fOpt   = curSel.find('option').eq(0),
				sOpt   = curSel.find('option').eq(1);
			if( fOpt !== undefined ){
				setTimeout(() => {
					if( fOpt.val()[0] == '?' ){
						fOpt.remove();
						sOpt.prop('selected', true);
					}
				}, 1);
			}
		});
	}*/
	if( tabs.length ) {
		let fTab = tabs.find('a:eq(0)');
			fTab.addClass('active-tab');
	}
	if( slider.length ) {
		setTimeout(() => {
			let startHeight = slider.find('img.slide:eq(0)').height();
			slider.css('height', startHeight);
			if( isAndroid42 ){
				if( $(window).height() > $(window).width() ){
					slider.css('height', '88px');
					slider.find('img.slide').css('height', '88px');
				} else {
					slider.css('height', '132px');
					slider.find('img.slide').css('height', '132px');
				}
			}
			window.onresize = windowResize;
		}, 1);
	}
	if( isIOS && redB.length ) {
		redB.hover(() => {
			$(this).css('borderBottom', '2px solid #A11015');
		}, () => {
			$(this).css('borderBottom', '5px solid #A11015');
		})
	}
	if( radio.length ){
		radio.eq(1)[0].checked = false;
	}
	if( radioP.length ){
		//radioP.eq(1)[0].checked = false;	
	}

	function windowResize() {
		let imgHeight = slider.find('img.slide:eq(0)').height();
		slider.css('height', imgHeight);
		if( isAndroid42 ){
			if( $(window).height() > $(window).width() ){
				slider.css('height', '88px');
				slider.find('img.slide').css('height', '88px');
			} else {
				slider.css('height', '132px');
				slider.find('img.slide').css('height', '132px');
			}
		}
	}

	function detectSafari(argument) {
		var ua = navigator.userAgent.toLowerCase(); 
		if (ua.indexOf('safari') != -1) { 
			if (ua.indexOf('chrome') > -1) {
				return false;
			} else {
				return true;
			}
		}
	}

});
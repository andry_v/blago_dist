(() => {
	'use strict';
		
	angular
		.module('app', [
			'ngAnimate',
			'ngHamburger',
			'ui.bootstrap',
			'ui.mask',
			'ngMap',
			'angular-carousel',
			'ngMockE2E'
		]);

})();
(() => {

	angular
		.module('app')
		.controller('MapCtrl', mapController)

	function mapController($scope, NgMap, $uibModal, formHttp, spinnerService, $interpolate) {

		var vm = this;

		vm.mapInitiation 	= false;

		vm.showSearchTitle 	= false;

		vm.googleMapsUrl 	= 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDEkVB4nZR44FqQYuvVo88Xgp6-ASfycSo';

		vm.region  			= 'all';

		vm.city	  			= 'all';

		vm.markers 			= [];

		$scope.mapItems		= [/*{
			officeNumber: 1,
			text: 'kek'
		}, {
			officeNumber: 2,
			text: 'kek'
		}, {
			officeNumber: 3,
			text: 'kek'
		}, {
			officeNumber: 4,
			text: 'kek'
		}, {
			officeNumber: 5,
			text: 'kek'
		}, {
			officeNumber: 6,
			text: 'kek'
		}*/];

		$scope.mapItemsPage		= 1;

		$scope.mapItemsPerPage	= 10;

		$scope.mapItemsMaxSize  = 3;

		vm.initMapMarkers = (spinState = true) => {
			if (spinState)
				spinnerService.show('mapSpinner');
			NgMap.getMap().then(map => {
				vm.markers.forEach((marker) => {
					marker.setMap(null);
				});
			    formHttp.getMarkers({
					region: vm.region,
					city: vm.city
				}).then(markersData => {
					
			    	let icon = {
			    		url: 'img/map_icon.png',
			    		size: new google.maps.Size(91, 55),
			    		origin: new google.maps.Point(0,0),
			    		anchor: new google.maps.Point(0,32)
			    	};

			    	vm.markers = markersData.data.map(mark => {
			    		let marker = new google.maps.Marker({
			    			position: mark.coords,
						    map: map,
						    icon: icon,
						    title: 'Blago',
						    zIndex: 3
			    		});

			    		marker.addListener('click', () => {
							vm.openModal(mark);
						});

			    		marker.setMap(map);

			    		$scope.mapItems.push({
			    			officeNumber: mark.offNum,
			    			text: mark.text
			    		});

			    		return marker;
			    	});
			    }, err => {
			    	console.log(err);
			    });
			}).finally(() => {
		    	spinnerService.hide('mapSpinner');
		    });
		}

		vm.openModal = opt => {
			let url = 'templates/map.modal.html';
			let modalInstance = $uibModal.open({
				templateUrl: url,
				resolve: {
					options: () => {
						return opt;
					}
				},
				controller: ModalInstanceCtrl
			});
		}

		vm.loadMap = () => {
			vm.mapInitiation = true;
		}

		function ModalInstanceCtrl($scope, $uibModalInstance, options, $compile){
			$scope.image		= '<img ng-src="'+options.img+'">';
			$scope.text 		= options.text;
			$scope.officeNumber = options.offNum;

			$scope.close = () => {
				$uibModalInstance.close();
			};
		}
	}

})();
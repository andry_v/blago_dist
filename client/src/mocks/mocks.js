(() => {

	angular
		.module('app')
		.run(httpBackendMocks);

	httpBackendMocks.$inject = ['$httpBackend']

	function httpBackendMocks($httpBackend) {

		const types = [
			{
				id: 0,
				name: "Смартфоны",
				brands: [0, 1]
			}, {
				id: 1,
				name: "Планшеты",
				brands: [2, 3]
			}, {
				id: 2,
				name: "Фотоаппараты",
				brands: [4, 5]
			}, {
				id: 3,
				name: "Видеокамеры",
				brands: [6, 7]
			}, {
				id: 4,
				name: "Телевизоры",
				brands: [8, 9]
			}, {
				id: 5,
				name: "GPS-навигатор",
				brands: [10, 11]
			}
		];

		const brands = [
			{
				id: 0,
				name: 'Majestic',
				models: [0, 1]
			}, {
				id: 1,
				name: 'Acer',
				models: [2, 3]
			}, {
				id: 2,
				name: 'WinBook',
				models: [4, 5]
			}, {
				id: 3,
				name: 'Prestigio',
				models: [6 , 7]
			}, {
				id: 4,
				name: 'Samsung',
				models: [8, 9]
			}, {
				id: 5,
				name: 'Shan-Tzung',
				models: [10, 11]
			}, {
				id: 6,
				name: 'Xiomi',
				models: [12, 13]
			}, {
				id: 7,
				name: 'Meizu',
				models: [14, 15]
			}, {
				id: 8,
				name: 'Lenovo',
				models: [16, 17]
			}, {
				id: 9,
				name: 'BrandX',
				models: [18, 19]
			}, {
				id: 10,
				name: 'BrandY',
				models: [20, 21]
			}, {
				id: 11,
				name: 'Toshiba',
				models: [22, 23]
			}
		];

		const models = [
			{
				id: 0,
				name: 'TAB 173 3G',
				price: 128
			}, {
				id: 1,
				name: 'Z200',
				price: 228
			}, {
				id: 2,
				name: 'Z500',
				price: 322
			}, {
				id: 3,
				name: 'Z520',
				price: 433
			}, {
				id: 4,
				name: 'SSDD 435',
				price: 153
			}, {
				id: 5,
				name: 'Hatsune X',
				price: 523
			}, {
				id: 6,
				name: 'Miku',
				price: 840
			}, {
				id: 7,
				name: 'ZVAI 228',
				price: 164
			}, {
				id: 8,
				name: 'Nibiru',
				price: 947
			}, {
				id: 9,
				name: 'SLV 427',
				price: 563
			}, {
				id: 10,
				name: 'STR 777',
				price: 153
			}, {
				id: 11,
				name: 'Komma x3',
				price: 918
			}, {
				id: 12,
				name: 'Komma x6',
				price: 943
			}, {
				id: 13,
				name: 'Komma x7',
				price: 1046
			}, {
				id: 14,
				name: 'Komma x8',
				price: 1492
			}, {
				id: 15,
				name: 'Komma x9',
				price: 1894
			}, {
				id: 16,
				name: 'Komma x10',
				price: 100
			}, {
				id: 17,
				name: 'Komma x11',
				price: 102
			}, {
				id: 18,
				name: 'Komma x12',
				price: 104
			}, {
				id: 19,
				name: 'Komma x13',
				price: 105
			}, {
				id: 20,
				name: 'Komma x14',
				price: 106
			}, {
				id: 21,
				name: 'Komma x15',
				price: 107
			}
		];

		const regInfo = [
			{
				name: "Киевская",
				cities: [{
					name: "Киев",
					offices: [1, 2]
				}, {
					name: "Глеваха",
					offices: [3, 4]
				}]
			}, {
				name: "Винницкая",
				cities: [{
					name: "Киев",
					offices: [5, 6]
				}, {
					name: "Глеваха",
					offices: [7, 8]
				}]
			}, {
				name: "Житомирская",
				cities: [{
					name: "Киев",
					offices: [1, 2]
				}, {
					name: "Глеваха",
					offices: [3, 4]
				}]
			}, {
				name: "Ровенская",
				cities: [{
					name: "Киев",
					offices: [1, 2]
				}, {
					name: "Глеваха",
					offices: [3, 4]
				}]
			}, {
				name: "Донбас",
				cities: [{
					name: "Киев",
					offices: [1, 2]
				}, {
					name: "Глеваха",
					offices: [3, 4]
				}]
			}];

		const markers = [
				{
					id: 0,
					offNum: 234,
					coords: {
						lat: 50.503015,
						lng: 30.498213
					},
					img: 'img/blago_punkt.JPG',
					text: 'вулиця Челюскінців, 34 <br />Пн-Сб 8:00-19:00 Вс 8-1 <br />Золото, серебро, техника'
				}, {
					id: 1,
					offNum: 235,
					coords: {
						lat: 50.504815,
						lng: 30.493613
					},
					img: 'img/blago_punkt_1.JPG',
					text: 'вулиця Челюскінців, 25 <br />Пн-Сб 8:00-19:00 Вс 8-1 <br />Золото, серебро, техника'
				},	{
					id: 2,
					offNum: 236,
					coords: {
						lat: 50.502415,
						lng: 30.494513
					},
					img: 'img/blago_punkt_2.JPG',
					text: 'вулиця Челюскінців, 11 <br />Пн-Сб 8:00-19:00 Вс 8-1 <br />Золото, серебро, техника'
				},	{
					id: 3,
					offNum: 236,
					coords: {
						lat: 50.402415,
						lng: 30.594513
					},
					img: 'img/blago_punkt_2.JPG',
					text: 'вулиця Челюскінців, 11 <br />Пн-Сб 8:00-19:00 Вс 8-1 <br />Золото, серебро, техника'
				},	{
					id: 4,
					offNum: 236,
					coords: {
						lat: 50.502415,
						lng: 30.694513
					},
					img: 'img/blago_punkt_2.JPG',
					text: 'вулиця Челюскінців, 11 <br />Пн-Сб 8:00-19:00 Вс 8-1 <br />Золото, серебро, техника'
				},	{
					id: 5,
					offNum: 236,
					coords: {
						lat: 50.702415,
						lng: 30.894513
					},
					img: 'img/blago_punkt_2.JPG',
					text: 'вулиця Челюскінців, 11 <br />Пн-Сб 8:00-19:00 Вс 8-1 <br />Золото, серебро, техника'
				},	{
					id: 6,
					offNum: 236,
					coords: {
						lat: 50.302415,
						lng: 30.394513
					},
					img: 'img/blago_punkt_2.JPG',
					text: 'вулиця Челюскінців, 11 <br />Пн-Сб 8:00-19:00 Вс 8-1 <br />Золото, серебро, техника'
				},	{
					id: 7,
					offNum: 236,
					coords: {
						lat: 50.402415,
						lng: 30.494513
					},
					img: 'img/blago_punkt_2.JPG',
					text: 'вулиця Челюскінців, 11 <br />Пн-Сб 8:00-19:00 Вс 8-1 <br />Золото, серебро, техника'
				},	{
					id: 8,
					offNum: 236,
					coords: {
						lat: 50.502415,
						lng: 30.594513
					},
					img: 'img/blago_punkt_2.JPG',
					text: 'вулиця Челюскінців, 11 <br />Пн-Сб 8:00-19:00 Вс 8-1 <br />Золото, серебро, техника'
				},	{
					id: 9,
					offNum: 236,
					coords: {
						lat: 50.52415,
						lng: 30.294513
					},
					img: 'img/blago_punkt_2.JPG',
					text: 'вулиця Челюскінців, 11 <br />Пн-Сб 8:00-19:00 Вс 8-1 <br />Золото, серебро, техника'
				}
			];

		$httpBackend.whenGET(/\/markers$/).respond((method, url, data) => {
				return [200, markers, {}];
		});

		$httpBackend.whenGET(/^(?=.*\bregion=all\b)(?=.*\bcity=all\b).*$/m).respond((method, url, data) => {
			var params = parseEncoded(url);
				return [200, markers, {}];
		});

		$httpBackend.whenGET(/markers/).respond((method, url, data) => {
			var res = getOffices(url);
			console.log(res);
			return [200, res, {}];
		});
		
		$httpBackend.whenGET(/\/techTypes$/).respond((method, url, data) => {
			return [200, getTechTypes(), {}];
		});

		// Get brands
		$httpBackend.whenGET(/(techTypes\/[0-9]?[0-9])+$/).respond((method, url, data) => {
			var params = parseParam(url);
			return [200, getBrandTypes(params), {}];
		});

		// Get models
		$httpBackend.whenGET(/(brands\/[0-9]?[0-9])+$/).respond((method, url) => {
			var params = parseParam(url);
			return [200, getModelTypes(params), {}];
		});

		//$httpBackend.whenGET(/^templates\//).passThrough();

		$httpBackend.when('GET', /\.html$/).passThrough()

		$httpBackend.whenPOST(/^\/quest/).respond((method, url, data, headers) => {
			return [200, {}, {}];
		});

		$httpBackend.whenPOST(/^\/quiz/).respond((method, url, data, headers) => {
			return [200, {}, {}];
		});

		function parseEncoded(val) {
		    var result = "Not found",
		        tmp = [];
		    console.log(val.substr());
		    return val
		    	.substr((val.indexOf('?') + 1))
		        .split("&")
		        .map(function (item) {
		        	tmp = item.split("=");
		        	var res = {};
		        	res[tmp[0]] = decodeURIComponent(tmp[1]);
		        	return res;
		    	});
		};

		function parseParam(val) {
			val = val
				.substr(1);
			return val
				.substr((val.indexOf('/') + 1));
		}

		function getTechTypes(){
			return _.map(types, item => {
				return { id: item.id, name: item.name };
			});
		};

		function getBrandTypes(techType) {
			let ids = _.findWhere(types, {
				id: parseInt(techType)
			}).brands;
			return _.reduce(brands, (mem, item) => {
				return _.indexOf(ids, item.id) != -1 ? mem.concat(item) : mem;
			}, []);
		};

		function getModelTypes(brandType) {
			let ids = _.findWhere(brands, {
				id: parseInt(brandType)
			}).models;
			return _.reduce(models, (mem, item) => {
				return _.indexOf(ids, item.id) != -1 ? mem.concat(item) : mem;
			}, []);
		}

		function findModelsByTypeId(typeId) { 
			return _.reduce(typesState, (mem, val, key) => {
				if( val.id == typeId )
					return mem.concat(val.models);
				else
					return mem.concat([]);
			}, []);
		}

		function findCities(region) {
			return regInfo.filter((item) => {
				return item.name == region ? true : false;
			});
		}

		function findOffices(arr, city) {
			return arr.filter((item) => {
				return item.name == city ? true : false;
			})[0].offices;
		}

		function getOffices(url) {
			var params = parseEncoded(url);
			var cities = findCities(params[1].region);
			var offices = findOffices(cities[0].cities, params[0].city);
			
			var res = markers.filter((item) => {
				for(var i = 0; i < offices.length; i++){
					if( item.id == offices[i] ) {
						return true;
					}
				}
			});

			return res;
		}
	}

})();
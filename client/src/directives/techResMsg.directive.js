(() => {

	angular
		.module('app')
		.directive('techResMsg', techResMsg);

	function techResMsg($compile){
		return {
			restrict: 'AE',
			scope: {
				errState: '=',
				err: '='
			},
			link: link
		};

		function link(scope, element, attrs) {
			scope.$watch("errState", val => {
	        	if( !val ) {
					let wrapper = angular.element('<div>');
					wrapper.html(getTechEvalErrTpl(scope.err));

					let tplFn = $compile(wrapper.contents());
					let child = tplFn(scope);

					element.append(child);
	        	}
	        });

			function getTechEvalErrTpl(errs){
				if ( !errs ) return;
				
				var hasEl = [],
					html,
					sLength;
				
				html = '<p>Просчет невыполнен, ';
				sLength = html.length;

				if( errs.indexOf('techType') > -1 ) {
					html += 'укажите вид техники';
					hasEl.push('techType');
				}
				if( errs.indexOf('techModel') > -1 ) {
					if( hasEl.length && haveNextSublings(errs, errs.indexOf('techModel')) ){
						html += ', модель ';
					} else {
						if( hasEl.length ){
							html += 'и модель';
						} else {
							html += 'укажите модель ';
						}
					}

					hasEl.push('techModel');
				}
				if( errs.indexOf('exState') > -1 ) {
					if( hasEl.length ){
						html += 'и внешний вид';
					} else {
						html += 'укажите внешний вид';
					}
				}

				if( html.length > sLength ){
					html += '</p>';
					return html;
				}
			}

			function haveNextSublings(arr, i){
				return  (typeof arr[i+1] !== undefined ? true : false) ||
						(typeof arr[i+2] !== undefined ? true : false) ||
						(typeof arr[i+3] !== undefined ? true : false);
			}
		}
	}

})();
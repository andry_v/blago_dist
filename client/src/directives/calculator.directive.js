(() => {

	angular
		.module('app')
		.directive('calc', calculator);

	function calculator(){
		return {
			restrict: 'AE',
			scope: {
				model: '='
			},
			template: [
				'<div class="calc-res">',
					'{{ model | number }} грн.',
				'</div>'
			].join(''),
			link: (scope, element, attrs) => {
				scope.$watch("model", value => {
	                
	            });
			}
		};
	}

})();
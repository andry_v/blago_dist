(() => {

	angular
		.module('app')
		.directive('ngAppend', ngAppend);

	function ngAppend($compile, $rootScope){
		return {
			link: link,
			scope: {
				ngAppend: '='
			}
		}

		function link(scope, element, attrs){
			var wrapper = angular.element('<div>');
			wrapper.html(scope.ngAppend);
			var tplFn = $compile(wrapper.contents());
			var child = tplFn(scope);
			element.append(child);
		}
	}

})();
(() => {

	angular
		.module('app')
		.directive('phoneNumber', phoneNumber);
		/*
		Intended use:
			<phonenumber-directive placeholder='prompt' model='someModel.phonenumber'></phonenumber-directive>
		Where:
			someModel.phonenumber: {String} value which to bind only the numeric characters [0-9] entered
				ie, if user enters 617-2223333, value of 6172223333 will be bound to model
			prompt: {String} text to keep in placeholder when no numeric input entered
		*/
		function phoneNumber($filter){
			return {
				link: link,
				restrict: 'E',
				scope: {
					phonenumberPlaceholder: '=placeholder',
					phonenumberModel: '=model',
				},
				template: '<input ng-model="inputValue" type="tel" class="phonenumber" placeholder="{{phonenumberPlaceholder}}" title="Phonenumber (Format: (999) 9999-9999)">',
			};

			function link(scope, element, attributes) {
				// scope.inputValue is the value of input element used in template
				scope.inputValue = scope.phonenumberModel;
	 
				scope.$watch('inputValue', function(value, oldValue) {
					
					value = String(value);
					var number = value.replace(/[^0-9]+/g, '');
					scope.phonenumberModel = number;
					scope.inputValue = $filter('telephone')(number);
				});
			}

			function setCaretPosition(elem, caretPos) {
		        if (elem !== null) {
		            if (elem.createTextRange) {
		                var range = elem.createTextRange();
		                range.move('character', caretPos);
		                range.select();
		            } else {
		                if (elem.selectionStart) {
		                    elem.focus();
		                    elem.setSelectionRange(caretPos, caretPos);
		                } else
		                    elem.focus();
		            }
		        }
		    }
		}

})();
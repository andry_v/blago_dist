(() => {

	angular
		.module('app')
		.directive('compileMap', compileMap);

	function compileMap($compile, $rootScope) {
		return {
			restrict: 'AE',
			scope: {
				compileMap: '=',
				initMarkers: '&'
			},
			link: link
		}

		function link(scope, element, attrs){
			scope.$watch('compileMap', val => {
				if( val ){
					let wrapper = angular.element('<div>');
					wrapper.html(getMapTemplate());

					let tplFn = $compile(wrapper.contents());
					let child = tplFn(scope);

					element.append(child);
					scope.initMarkers();
				}
			});
		}

		function getMapTemplate() {
			return [
				'<div id="map" map-lazy-load="https://maps.google.com/maps/api/js"',
						'map-lazy-load-params="https://maps.googleapis.com/maps/api/js?key=AIzaSyDEkVB4nZR44FqQYuvVo88Xgp6-ASfycSo">',
						'<ng-map trigger-resize="true" center="50.503015, 30.498213" zoom="10"></ng-map>',
				'</div>',
			].join('');
		}
	}

})();
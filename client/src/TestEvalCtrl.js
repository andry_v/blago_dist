(() => {

	angular
		.module('app')
		.controller('TestEvaluationCtrl', TestEvalCtrl);

	TestEvalCtrl.$inject = ['$scope', '$uibModal', 'formHttp', 'formValidation'];

	function TestEvalCtrl($scope, $uibModal, formHttp, formValidation) {
		const vm = this;

		formValidation.setScope($scope);
		formValidation.setForm('evalForm');

		vm.searchOffice = false;
		vm.userInfo = true;
		vm.techErrState = true;
		vm.techErr = [];

		vm.techTypes = [{
			id: 0,
			name: 'Выберите тип',
			disabled: true
		}];
		vm.techBrands = [{
			id: 0,
			name: 'Выберите бренд',
			disabled: true
		}, {
			id: 1,
			name: 'Супер бренд',
			disabled: false
		}, {
			id: 0,
			name: 'Ахуенный бренд',
			disabled: false
		}];
		vm.techModels = [{
			id: 0,
			name: 'Выберите модель',
			price: 0,
			disabled: true
		}];

		$scope.techType = {
			id: 0,
			name: 'Выберите тип',
			disabled: true
		};
		//$scope.techBrand = vm.techBrands[0];
		$scope.techModel = true;

		/*function getTechTypes(){
			return formHttp.getTechTypes().then(data => {
				vm.techTypes = data.map(item => {
					item.disabled = false;
					return item;
				});
				vm.techTypes.unshift({id: -1, name: 'Выберите вид', disabled: true});
				$scope.evaluation.techType = vm.techTypes[0];
				//return vm.techTypes;
			});.then(data => {
				setTimeout(() => {
					setInputInvalid('techType');
					setInputInvalid('techBrand');
					setInputInvalid('techModel');
				}, 1);
				return data;
			});
		}*/

		/*function setInputInvalid(input) {
			$scope.evalForm[input].$setValidity('required', false);
		}*/

		/*vm.getBrandsByType = type => {
			return formHttp.getBrandsByType(type.id).then(data => {
				vm.techBrands = data.map((item) => {
					item.disabled = false;
					return item;
				});

				vm.techBrands.unshift({id: -1, name: 'Выберите бренд', disabled: true});
				$scope.evaluation.techBrand = vm.techBrands[0];

			});.then(data => {
				setTimeout(() => {
					setInputInvalid('techBrand');
				}, 1);
				return data;
			});
		}*/

		/*vm.getModelsByBrand = brand => {
			return formHttp.getModelsByBrand(brand.id).then(data => {
				vm.techModels = data.map((item) => {
					item.disabled = false;
					return item;
				});

				vm.techModels.unshift({id: -1, name: 'Выберите модель', price: 0, disabled: true});
				$scope.evaluation.techModel = vm.techModels[0];
			});
		}*/

		//getTechTypes();
	}

})();
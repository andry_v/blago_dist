'use strict';

module.exports = function() {
   var ngModules = [
       './client/vendors/angular/angular.js',
       './client/vendors/angular-animate/angular-animate.js',
       './client/vendors/angular-touch/angular-touch.js',
       './client/vendors/angular-bootstrap/ui-bootstrap.js',
       './client/vendors/angular-bootstrap/ui-bootstrap-tpls.js',
       './client/vendors/angular-mocks/angular-mocks.js',
       './client/vendors/angular-hamburger-toggle/dist/angular-hamburger-toggle.js',
       './client/vendors/angular-http-loader/app/package/js/angular-http-loader.js',
       './client/vendors/angular-carousel/dist/angular-carousel.min.js',
       './client/vendors/angular-ui-mask/dist/mask.js',
       './client/vendors/ngmap/build/scripts/ng-map.js'
   ];

   var vendors = {
       scripts: ngModules.concat([
          './client/vendors/jquery/dist/jquery.js',
          './client/vendors/underscore/underscore.js'
       ]),
       styles: [
          './client/fonts/fonts.css',
          './client/vendors/angular-hamburger-toggle/dist/angular-hamburger-toggle.css',
          './client/vendors/angular-bootstrap/ui-bootstrap-csp.css',
          './client/vendors/bootstrap/dist/css/bootstrap.css',
          './client/vendors/angular-carousel/dist/angular-carousel.css'
       ]
   };

    var libs = [
      './client/vendors/babel-polyfill/browser-polyfill.js'
    ];

    var scss = [
      './client/vendors/include-media/dist/_include-media.scss'
    ];
   
    var components = [
        './client/src/app/app.js',
        './client/src/**/*.js',
        './client/src/*.js',
        '!./client/src/**/*.spec.js'
    ];

   return {
     paths: {
         src: {
             vendors: {
                ngModules: ngModules,
                scripts: vendors.scripts,
                styles: vendors.styles,
                libs: libs,
                scss: scss
             },
             components: components,
         }
     }
   };
};
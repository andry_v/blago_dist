'use strict';

var runSequence = require('run-sequence');

gulp.task('dev', function(callback){
    runSequence('scripts', 'styles', ['images', 'watch'], callback);
});

gulp.task('test_ports', function(){
	console.log('browserSync port: ', process.env.BROWSER_SYNC_PORT, "\n");
	console.log('server port: ', process.env.SERVER_PORT, "\n");
});

gulp.task('serve', ['dev'], function(){
    browserSync.init(null, {
        proxy: "http://localhost:" + process.env.SERVER_PORT,
        files: ["./dist/"],
        browser: "google chrome",
        port: process.env.BROWSER_SYNC_PORT
    });

    gulp.watch('./dist/evaluation.html').on('change', browserSync.reload);

});
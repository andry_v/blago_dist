'use strict';

var runSequence = require('run-sequence');
var ftp			= require('vinyl-ftp');
var gutil 		= require('gulp-util');

gulp.task('deploy', function(cb){
	runSequence('scripts', 'styles', 'deploy:ftp', 'deploy:watch', cb);
});

gulp.task('deploy:watch', function(){
	gulp.watch('client/src/**/*.js', ['deploy:scripts']);
    gulp.watch(['client/scss/**/*.scss','client/src/**/*.scss'], ['deploy:styles']);
    gulp.watch('dist/evaluation.html', ['deploy:html']);
});

gulp.task('deploy:scripts', function(cb){
	runSequence('lint', 'scripts', 'deploy:ftp', cb);
});

gulp.task('deploy:styles', function(cb){
	runSequence('styles', 'deploy:ftp', cb);
});

gulp.task('deploy:html', function(cb){
	runSequence('html', 'scripts', 'deploy:ftp', cb);
});

gulp.task('deploy:ftp', function(){

	var connection = ftp.create({
		host: '194.28.86.115',
		user: 'chiskiev',
		password: '1218145Y35',
		log: gutil.log
	});
	
	var files = [
		'./dist/*.html',
		'./dist/app.js',
		'./dist/app.css',
		'./dist/templates/**'
	];

	
	return gulp.src( files, { base: './dist/', buffer: false } )
			   .pipe( connection.dest('/public_html/blago/') );

});

gulp.task('deploy:release', function(){

	var connection = ftp.create({
		host: '194.28.86.115',
		user: 'chiskiev',
		password: '1218145Y35',
		log: gutil.log
	});
	
	var files = [
		'./release/*.html',
		'./release/app.js',
		'./release/app.css',
		'./release/templates/**',
		'./release/fonts/**',
		'./release/img/**'
	];

	
	return gulp.src( files, { base: './release/', buffer: false } )
			   .pipe( connection.dest('/public_html/blago/') );

});
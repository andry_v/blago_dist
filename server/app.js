var app 			= require('express')();
var path			= require('path');
var routes  		= require('./routes/index');
var bodyParser 		= require('body-parser');
var methodOverride  = require('method-override');

require('dotenv').load();

app.use(express.static(path.join(__dirname, "../dist")));
app.use(express.static(path.join(__dirname, "./static")));
app.use(express.static(path.join(__dirname, "./img")));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use('/', routes);

app.use(methodOverride());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(function(err, req, res, next){
    res.status(500).send({err: err.stack});
});

var port = process.env.SERVER_PORT;

app.listen(port, function(){
	console.log('"BLAGO Lombard" project listening on port: ', port);
});
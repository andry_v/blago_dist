router.post('/quest', (req, res) => {
	var data = req.params;

	setTimeout(() => {
		if( !data || !data.email ) {
			//res.send('Ta tyt pizda, bu cho bl9t');
			res.send(req.params);
		} else {
			res.send('Ebaaat, vse norm');
		}
	}, 1000);

});

router.get('/quest/:questId', (req, res) => {
	var data = req.params;

	if( !data || !data.email ) {
		res.send('Ta tyt pizda, bu cho bl9t');
	} else {
		res.send('Ebaaat, vse norm');
	}

});

router.get('/markers', (req, res) => {
	setTimeout(() => {
		if( req.query.region == 'all' && req.query.city == 'all' ){
			var markers = [
				{
					id: 0,
					offNum: 234,
					coords: {
						lat: 50.503015,
						lng: 30.498213
					},
					img: 'img/blago_punkt.JPG',
					text: ['вулиця Челюскінців, 34 <br />',
							'Пн-Сб 8:00-19:00 Вс 8-1 <br />',
							'Золото, серебро, техника'].join('')
				},
				{
					id: 1,
					offNum: 235,
					coords: {
						lat: 50.504815,
						lng: 30.493613
					},
					img: 'img/blago_punkt_1.JPG',
					text: ['вулиця Челюскінців, 25 <br />',
							'Пн-Сб 8:00-19:00 Вс 8-1 <br />',
							'Золото, серебро, техника'].join('')
				},
				{
					id: 2,
					offNum: 236,
					coords: {
						lat: 50.502415,
						lng: 30.494513
					},
					img: 'img/blago_punkt_2.JPG',
					text: ['вулиця Челюскінців, 11 <br />',
							'Пн-Сб 8:00-19:00 Вс 8-1 <br />',
							'Золото, серебро, техника'].join('')
				}
			];
		} else {
			var markers = [
				{
					id: 0,
					offNum: 123,
					coords: {
						lat: 50.503515,
						lng: 30.493213
					},
					img: 'img/blago_punkt.JPG',
					text: ['вулиця Челюскінців, 4 <br />',
							'Пн-Сб 8:00-19:00 Вс 8-1 <br />',
							'Золото, серебро, техника'].join('')
				},
				{
					id: 1,
					offNum: 321,
					coords: {
						lat: 50.501515,
						lng: 30.497213
					},
					img: 'img/blago_punkt_1.JPG',
					text: ['вулиця Челюскінців, 2 <br />',
							'Пн-Сб 8:00-19:00 Вс 8-1 <br />',
							'Золото, серебро, техника'].join('')
				}
			];
		}
		res.send(markers);
	}, 1000);
});
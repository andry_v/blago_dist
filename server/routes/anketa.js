router.get('/form', (req, res) => {
	var options = {
		root: __dirname + '/../../dist/',
		dotfiles: 'deny',
		headers: {
		    'x-timestamp': Date.now(),
		    'x-sent': true
		}
	};
	
    var fileName = 'anketa.html';
	res.sendFile(fileName, options, err => {
		if( err ){
			console.log(err);
		}
	});
});